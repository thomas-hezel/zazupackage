.. zazupackage documentation master file, created by
   sphinx-quickstart on Sun Mar 31 20:01:01 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

################################
zazupackage 1.1.0 Documentation
################################
.. toctree::
   :maxdepth: 2
   :caption: Contents:::


******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

|
|
|

*****
Whois
*****

As readme.rst file on bitbucket, this documentation looks not perfect. Not everything
is rendered correctly. Better check "Documentation/build/html/index.html"

:Version: v1.1.0 20190509
:Author: Thomas Hezel

|
|

Open Tasks
==========
- build .md markdown files for bitbucket readme.md since readme.rst is not rendered correct
- propper yaml sequence for CKEditor and DCE / page TSConfig

|
|
|




*************************
Start a new TYPO3 website
*************************

Server Setup
============

1. Terminal: make a folder where everything goes to
2. give the folder the rights "2775" and the owner "yourTerminalUser:webServiceUser" ("2" in front is important!)
3. put the basic composer.json into this folder
4. run inside this folder "composer install"
5. cd inside the public folder and do: "touch FIRST_INSTALL"
6. get your Database connection details (user and password) / you can create a DB or let TYPO3-FIRST_INSTALL do it
7. with the Browser navigate into the public folder and do TYPO3 first install …
8. if you followd the above you should end up in the new backend

TYPO3 Setup
===========

1. create your start page under the TYPO3 logo: "Home"
2. Template -> Home: "Create template for a new site"
3. Info/Modify under Home -> "Edit the whole template record"
4. Template Title -> "My root template"
5. Delete what is in Setup
6. Includes: fluid-styled-content, form, shariff (FE, no jQ, no FontAw), news, cookies, LAST!: zazupackage
7. Home -> edit page - Appearance: Backendlayout plus subpages  (BE 1 col)
8. Home -> edit page - Behavior: "Use as Root Page"
9. Save and Flush all Caches
10. Sites: "Add new site configuration for this site" -> do all settings
11. go to Home and test it - you should see the default header and the footer
12. got to Template -> Constants put in a new path for the new page Templates, Partials, Layouts

Admin Tools section
===================

1. Manage Language Packs -> update
2. Settings -> Debug Settings -> go from "live" to "Debug" -> press Activate
3. Check "Password hashing settings" should be "Argon2i" -> some hosters don't have it yet so you need to make new admin pw after moving
4. Configure Installation Wide Options -> put SYS.systemLocale = de_DE.UTF-8
5. Environment - Directory Status: check rights


Some dirty work – to get your DCEs from your TYPO3 standard installation
========================================================================
1. export from your base installation with phpMyAdmin "tx_dce_domain_model_dce" and "tx_dce_domain_model_dcefield", uncheck "validate foreign key"
2. delete the two tables in the destination, your new website
3. import the exported .sql file, uncheck "validate foreign key" for import
4. go to Admin Tools -> Analyze Database Strukture: should be "green" if you use the same versions of extensions and TYPO3.
5. go to SYSTEM -> DB check -> Manage Referenz Index: "Update reference index" / will be very "red" -> update

News and Form Page IDs and Folders
==================================
1. install news plugin "list" and "singele"
2. install form plugin

|
|
|

********************************
What to do with the sitepackage?
********************************

Make an additional add-on sitepackage or copy the sitepackage?
==============================================================

If you start a new website you must start with individual settings (mainly CSS).

Where to put it?

The philosophy is: Nothing that is not user is NOT in fileadmin.

So the zazupackage is idividual for evey website -> resulting in heavy update problems

The solution is to create an additionl add-on-sitepackage for each website, that holds the
individual settings of that website and keep the zazupackage/sitepackae general and a base for all websites.

So one more sitepackage!

 a. general sitepackage(zazupackage)
 b. unique sitepackage for the individual website (myWebsitePackage)



Get Started with a new Add-on-Sitepackage-Extension
===================================================

1. Kickstart a additional sitepackage to the zazupackage
    https://sitepackagebuilder.com
2. build a "Documentation" Folder and cd in the folder and "sphinx-quickstart"
    a. Separate source and build directories (y/n) [n]: y
    b. Name prefix for templates and static dir [_]:
    c. project name ...
    d. Project language [en]:
    e. Source file suffix [.rst]:
    f. Name of your master document (without suffix) [index]:
    g. autodoc: automatically insert docstrings from modules (y/n) [n]: y
    h. next 10 default: n
    i. Create Makefile? (y/n) [y]:
    j. Create Windows command file? (y/n) [y]: n
3. copy from zazupackage .editorconfig plus .gitignore
4. add to this new composer.json: "zazuberlin/zazupackage": "dev-master" as depedencie
5. built a Repository on bitbucket
6. setup git
    a. go inside the project folder an do: git init
    b. git status, git add . , git commit -m "initial commit"
    c. create release branch (master is default) "git branch release"
    d. add remote: "git remote add origin https://thomas-hezel@bitbucket.org/thomas-hezel/xxxxxx.git"
    e. use force push to overwrite everything on remote "git push -f"
7. add your new sitepackage-for-this-individual-site to the project-root composer.json
    a. add individual sitepackage and repository with ftp, or vim
    b. do "composer update" / this bitbucket is private so you have to give the password
8. commet all what is not needed in the Add-On-Package
    a. comment all that is inside "TSConfig/All.tsconfig"
    b. inside "ext_localconf.php" comment  RTE and the addPageTSConfig
    c. delete the file dynamicContent.typoscript
    d. empty inside of "constants.typoscript" and "setup.typoscript"
    e. add includeCSS for "overlay.css" of this extension inside setup.typoscript
    f. rename your Default.css to "overlay.css" this is the CSS special for this site
    g. commet evrything in "TCA/Overrides/pages.php" (no page TSconfig)
    h. the only thing what matters is "TCA/Overrides/sys_template.php" this loads the static temp-TypoScript
    i. delete the classes autoload part in "ext_emconf.php"
9. "git push" the updates and "composer update" them
10. copy settings from VisucalStudioCode to the new add-on-sitepackage
11. change FTP of VSC new add-on-sitepacke to the destinated typo3conf/ext/new-add-on-sitepackage

|
|
|

*********
Pre-Stuff
*********


|
|

Sphinx
======

Build a new directory ::

    mkdir Documentation
    cd Documentation
    sphinx-quickstart

more details:
https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html

change custom.css
put it in source/_static/
put the logo also in source/_static/
inside conf.py ::

    html_theme = 'classic'
    html_logo = '_static/logo-zazu-berlin.gif'
    html_style = 'custom.css'


command to render html - it will be in build/html/index.html ::

    make html

.. note:: zazu.berlin Headline Sequence: ## ** = - ^ "

|
|

Composer
========

Extension has composer.json

And a file for TYPO3 composer install [project]composer.json, this has to got into
the base folder for the TYPO3 project (delete the "[project]".
It will install all files and TYPO3 in a folder called public.
This folder needs later on write permissions for the server-process and ftp etc.

.. note::

  2019/April Extensions are only installed when the setup is in the root composer. They are not installed when they are in the zazupackage composer.json (exept of shariff).

.. code::

 composer install

and

.. code::

  composer update

The composer.json inside zazupackage should install all that zazupackage needs.

This will start getting an extension. But works only from root composer:
.. code::

  composer require vendor/extension


.. note:: If you get problems with composer updating or "not getting updates" or some similar warnings: DELETE the .composer in your home directory of the server.

Versions
--------
- "dev-master" will install the latest from master branch
- "^" - means only inside this main version
- "vendor/name" will install the latest version


Installing TYPO3 on a server
----------------------------

- the directory must have 775
- since the terminal user that executes composer is terminalUser:terminalUser, the group should be changed for the web-process (terminalUser:www-data)


.. warning::

  2019/April composer update is not working for the zazupackage ???

|
|

Directories and Files of the zazupackage
========================================
| \.vscode
|    settup.json - *for vsc project level* (*above is workspace then user*)
|    sftp.json - *for sftp vsc-extension*
|
| Classes
|     Controller - *is empty*
|     Domain - *is empty*
|     ViewHelpers - *is empty*
|
| Configuration
|     RTE
|         Default.yaml
|
|     TCA
|         Overrides
|             pages.php - *registers the file* **All.tsconfig** *witch then includes all* **TsConfig** *files*
|             sys_template.php - *puts the static template files with the TS in the BE list for including*
|
|     TsConf
|         Page
|             Mod
|                 WebLayout
|                     BackendLayouts
|                         zazu-bel.tsconfig - *config for the backend layout, generated in the BE*
|                     BackendLayouts.tsconfig - *includes the above file and all in the folder*
|         All.tsconfig - *include file for all .tsconfig below*
|         RTE.tsconfig - *empty*
|         TCEFORM.tsconfig - *empty*
|         TCEMAiN.tsconfig - *empty*
|
|     TypoScript
|          Helper
|              dynamicContent.typoscript - *lib to get stuff from tt_content left, middle etc.*
|              menues.typoscript - *deprecated, just archive, menu is now dataprocessor fluid*
|          constants.typoscript - *some are with #cat to work on in the BE, some not*
|          setup.typoscript - *this is the* **main TypoScript**
|          \.htaccess - *rquire all denied, blocks Apache to get access to the files*
|
| Documentation
|     build - *generated by Sphinx, usually HTML,CSS,Js files*
|     source
|         _static - *logo and css that is moved into build/_static while "make"*
|         _templates - *empty*
|         config.py - *python config file for the make process*
|         Makefile - *the make script*
|
| Resources
|     Private
|         Language
|             locallang_fe.xlf – *language file for the frontend, things outputed*
|             locallang_be.xlf – *language file for the backend*
|         Layouts
|             ContentElements - *empty since we work with DCEs*
|             Page
|                 Default-simple.html - *just the main menu and render section "main"*
|                 Default.html – *the MAIN Layout, renders section "main" in Default.html Template*
|         Partials
|             ContentElements - *empty - we use DCEs*
|             Page
|                 Navigation
|                     BreadcrumbItem.html – *one li-element for breadcrumb*
|                 Shariff.html – *which social media for shariff*
|         Templates
|             ContentElements
|                 Dce
|                     D1.html - *just a test, could hold the templates of the DCEs*
|             Page
|                 Default.html - *the MAIN Template with section "main" rendered by Layout*
|                 Two-Column.htm - *a test two-column version, auto chosen by backendLayouts*
|     Public
|         Css
|             Min – *minimalized css, for production mode*
|             Src – *normal css*
|                 01-normalize-801.css – *from the Normalize project version 8.0.1*
|                 02-boilerplate-701.css – *some basic settings from Boilerplate version 7.0.1*
|                 03-basic-styles.css – *from zazu: fonts, headlines, wrapper etc.*
|                 04-default-layout.css – *layout css: menu, header, IMPORT all other css*
|                 05-default-template.css – *template css: footer*
|                 06-dce.css – *this is the content through DCEs*
|                 07-all-content.css – *content not coming through DCEs*
|                 rte.css – *css for the RTE CKEditor*
|         Fonts – *all font folders*
|         Icons – *basically BE icons plus favicons*
|         Images – *images that belong directly to the template, content is in fileadmin*
|         JavaScript
|             Min – *minified versions*
|             Src – *normal js, while under construction*
|
| .editorconfig – *give different editors the same behavior*
| .gitignore – *things that are not relevant for the public*
| [project]composer.json – *composer.json to setup TYPO3 on a server - goes in Dest. folder*
| code-on-hold – *code storage for me - may use it somewhere*
| composer.json – *this is the json for this Extension!*
| ext_conf_template.txt – *empty, there is no conf. for the sitepackage*
| ext_emconf.php – *IMPORTANT version, name and dependencies of the sitepackage*
| ext_icon.png – *size 64x64*
| ext_localconf.php – *IMPORTANT includes All.tsconfig and RTE Default.yaml*
| ext_tables_static+adt.sql – *empty no database tables for sitepackage*
| ext_tables.php – *empty core: mostly no longer needed, TCA is in Configuration*
| ext_tables.sql – *empty sql table definitions*/

|
|
|

**********
Main-Stuff
**********
|
|

Basic Setup of TYPO3
====================

Settings in TYPO3 and php

  - setting systemLocale on install tool de_DE.UTF-8
  - configuration presets from live to debug
  - #show errors on frontend - must be set additional to debug, see next line
  - avoid white page at error: config.contentObjectExceptionHandler = 0

Fluidtemplate setup
-------------------

.. code:: typoscript

   // Part 1: Fluid template section
   10 = FLUIDTEMPLATE
   10 {
     templateName = TEXT
     templateName.stdWrap.cObject = CASE
     templateName.stdWrap.cObject {
        key.data = pagelayout

        pagets__site_package_default = TEXT
        pagets__site_package_default.value = Default

        default = TEXT
        default.value = Default
    }
     templateRootPaths {
        0 = EXT:site_package/Resources/Private/Templates/Page/
        1 = {$page.fluidtemplate.templateRootPath}
    }
     partialRootPaths {
        0 = EXT:site_package/Resources/Private/Partials/Page/
        1 = {$page.fluidtemplate.partialRootPath}
    }
     layoutRootPaths {
       0 = EXT:site_package/Resources/Private/Layouts/Page/
        1 = {$page.fluidtemplate.layoutRootPath}
     }
    }

CASE-Decision a function of stdWrap we deal with a cObject-Information
key.data is the CASE-key and data is some data from the data-function
otherwise it would be key.field for frontent Layout, pagelayout is BE-Layout Number
"default" belongs also to CASE
the $page variable is the variable of the page-constant (the page settings)



instead for FE-Layout-Number:
.. code:: typoscript

   key.field = layout
   1 = TEXT
   1.value = myPageLayoutDifferentFromDefault

instead for BE-Layout-Number
.. code:: typoscript

   key.data = pagelayout
   1 = TEXT
   1.value = myPageLayoutDifferentFromDefault


Old version direct to the template file
---------------------------------------
.. code:: typoscript

   10 = FLUIDTEMPLATE
   10 {
   template = FILE
   template.file = fileadmin/Resources/Private/Templates/mainTemplate.html
   …

    }


pagets__site_package_default
----------------------------

\pagets__\  is a pre-variable for backendlayout names set in the backendlayout
it is called with the function: PageTsBackendLayoutDataProvider

To set the TS-for backendLayouts not in a database-folder of the page-tree
but in files one has to make a file xx.tsconfig and then register it in a new
page.php file

Register static Page TSconfig files

Register PageTS config files in Configuration/TCA/Overrides/pages.php
of any extension,
which will be shown in the page properties (the same way as TypoScript
static templates are included):


.. code:: typoscript

  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'extension_name',
    'Configuration/TSconfig/Page/myPageTSconfigFile.txt',
     'My special config'
  );

All other methods that I found online didn't work!!!!
Kott has an additional comand in the ext_localconf.php but it seems not necessary!

The trick is to load one file in TsConf/Page/All.config
that has the include commands for all the other files in the folder

|
|

CONSTANTS
=========

Constants are set in the file: Configuration/TypoScript/constants.typoscript

then

inserted with a constant placement '$NAME/path ...' in setpu.typoscript

if

in constants.typoscript the comment after # is semantic ok they will be
in BE-Template-Constant-Editor for edit

can be used also then in Fluidtemplates {name.name}

|
|

RTE CKEditor configuration and individual config activation
===========================================================
| Good introduction
| https://www.nitsan.in/de/blog/post/einfache-schritte-zur-konfiguration-des-ckeditors-in-typo3

| Graphical interface to style the toolbar of the CKEditor
| https://ckeditor.com/latest/samples/toolbarconfigurator/index.html#basic

| homepage of the ckeditor
| https://ckeditor.com/docs/ckeditor4/latest/guide/dev_installation.html
| http://docs.ckeditor.com/#!/api/CKEDITOR.config


ext_localconf.php
    register with globas config_vars the preset
    "zazu" with a file source and name
    EXT:zazupackage/Configuration/RTE/Default-RTE-zazu.yaml

Configuration/RTE/Default-RTE-zazu.yaml
    file with the configuraton based on the default-setup changed for zazupackage

TsConfig
    Page/RTE.tsconfig will be included in All.tsconfig
    is setting the preset for all pages TSConfig to
    RTE.default.preset = zazu
    To change for a single page the preset must be written directly in the page TSConfig


CKEditor
--------
1. Step one: tags you can use to format ('a' doesn't work here!)

.. code:: yaml

    editor:
        config:
            format_tags: "p;h1;h2;h3;h4;h5;pre;address;div"



2. Menues to be shown

.. code:: yaml

   editor:
       config:
           toolbarGroups:
               - { name: clipboard, groups: [clipboard, undo] }
               - "/"
               - { name: styles }


3. add classes

.. code:: yaml

   editor:
       config:
           stylesSet:
               - { name: "quote-style", element: "p", attributes: { class: "quote-style"}}
               - { name: "Load More Button", element: "a", attributes: { class: "load-more"}}
               - { name: "More Content", element: "div", attributes: { class: "more-content"}}


4. To have a preview inside the CKEditor of a style, the style must be set in the rte.css

.. code:: yaml

    editor:
        config:
            contentsCss: "EXT:zazupackage/Resources/Public/Css/Src/rte.css"

shows a class or a styling inside the CKEditor in the intended way and
is not effecting output!!! if rte.css is not added to 04-default-layout.css


ToDo
----

But as soon as I set a RTE preset for the page (Page/RTE.tsconfig) the one in the DCE is overwritten.
The sequence should be from general to individual:
all pages -> this page -> DCE (contentElement) as strongest
But page overwrites the setting in the DCE!
issue to Vieweg

|
|

System Extension Form
=====================

Steps:
    1. add static Template (easily forgotten)
    2. add fluid_styled_content static Template (not sure whether realy needed)
    3. create /Configuration/Form-yaml/form-setup-zazu.yaml -> Path own Templates and Paritals
    4. TypoScript to tell the plugin (FE) and the module (BE) where to find the setup
    5. create Folder in Form-yaml with a Formdefinition created in BE -> transfer in EXT:
    6. add path to EXT: ... Forms/zazukontakt.form.yaml in form-setup-zazu.yaml

distinguish between:
    - plugin and module
    - Forms.yaml and form.config.yaml


.. note:: Add additional multi setups in the YAML can be made on the corresponding intend line!


Good Link for Howto own form.yaml new storage place:
  https://jweiland.net/typo3/codebeispiele/typoscript/ext-form-speicherort-der-formulare-festlegen.html

Confirmation Message
--------------------

Confirmation is not found directly in EXT:zazupackage through setup Template pathes.

.. note:: Confirmation has to be set inside the zazukontakt.form.yaml setup!

.. code::

    options:
      message: 'Vielen Dank für die Nachricht, wir werden uns umgehend bei Ihnen melden!'
      contentElementUid: ''
      templateName: Confirmation.html
      templateRootPaths:
        20: EXT:zazupackage/Resources/Private/Templates/FormTemplates/Finishers/Confirmation/
    identifier: Confirmation


Modifications and settings
--------------------------

Templates modified
    - Form.html - surrounding divs
    - add. kontakt-fields div for column
    - h2 Form name from EXT:LLL

Partials modified
    - Field.html - label class, div around input classes
    - label below imput
    - class getting field names in input classes: <div class="{element.properties.containerClassAttribute} {element.properties.containerClassAttribute}-{element.uniqueIdentifier}">

zazukontakt.form.yaml
    -deleted form label, to get it outside of kontakt fields for 2 coumun layout


To avoid that the headline (Form Name) is getting the two column out of balance
the h2 must go on top and the fields must get a div for the css column setting.
Form is rendering the fields and the label of the form (headline) in one go, so
the headline label has to be omitted in the setup.yaml and put inside the template
with a LLL Headline Name reference.


CSS column bugs
---------------
CSS avoid column break inside an element inconsistency in browsers ::

       .kontakt-fields {
        column-count: 2;
        column-gap: 80px;
        margin: 0;
        -webkit-column-break-inside: avoid;
        break-inside: avoid;
        break-inside: avoid-column;
    }

    .kontakt .form-group {
        -webkit-column-break-inside: avoid;
        -moz-column-break-inside: avoid;
        column-break-inside: avoid;
        break-inside: avoid-column;
        page-break-inside: avoid;
        display: inline-block;
    }


|
|

leaflet map and openstreetmap
=============================
Settins in DCE (.js) and setup.typoscript (css)
-----------------------------------------------

in the DCE 101 template
    - coordinates center of the map, lati - longi
    - location 1, lati1 - longi1
    - location 2, lati2 - longi2

in *Add-On-Sitepackage*: addOnSitePackage/Resources/Public/JavaScript/Src/leaflet-zazu.js
    - called in setup.typoscript only for the contact page
    - zoom factor
    - url of the pointer-logos "leaf-green.png", "leaf-shadow.png"
    - on pointer-logo-click message
    - osm or mapbox maps
    - jQuery activate button DSGVO - runs leaflet map

in Resources/Public/Ext/Leaflet-1.x.x
    - leaflet.js -> intergrated through DCE template
    - leaflet.css -> called in setup.typoscript only for the contact page (addOnSitePackage)


DSGVO-Sequence

  1. DCE creates js "var lati", js "var longi" etc.
  2. leaflet.css - loaded for the page in setup.typoscript condition (addOnSitePackage)
  3. leaflet-zazu.js - loaded for the page in setup.typoscript condition (addOnSitePackage)
  4. leaflet-zau.js - activates function with DSGVO button onclick (addOnSitePackage)
  5. leaflet-zazu.js - function loads the variable lati/longi and leaflet.js (addOnSitePackage)
  6. leaflet-zazu.js - hides DSGVO button, and static map background (addOnSitePackage)


|
|

jQuery and JavaScript
=====================

jQuery
    is included in the Templates/Page/Default.html (or whatever Template) at the bottom
    because Extensions (e.g. shariff) go usually on the top of "includeJSFooter"
    so would be before jQuery. And if jQuery is in the header Google is complaining:
    "no code before content"


JavaScript
    is included with "includeJSFooter" in setup.typoscript (only there TYPO3 will compress it)




|
|

News tx_news and news slider on Home called without plugin
==========================================================
Changed Templates and Partials
------------------------------

Templates
   - Templates/News/News/Detail.html (3 divs added, prev-next to bootom)
   - Templates/News/News/Detail.html (moved back-link into div of main text)
   - Templates/News/News/List.html (if template 9 or 10 etc.)



Partials
   – Partials/News/Detail/Shariff.html (choose social medias)

Partials/List
   - Item.html
   - new: ItemBrowseList.html (for the news FlexSlider)
   - new: ItemLatest.html (the layout 9 for Latest in the footer)
   - new: downloadItem.html (the layout for Gesitrel Downloads with news)

.. warning:: static templates - news must be included BEFORE zazupackage otherwise news settings in setup.typoscript will not come through

1. install news -> should now come from composer depedencie zazupackage
2. TypoScript/Helper/news.typoscript .lib = construct news List for slider
3. add DCE 26 news slider to home
4. make pages for "all news" and "single news" install plugin news on the pages

5. special list-template for the news slider



add to 2.
a. the lib inserts the news (like a plugin), and creates a template-case
b. in the fluid-template you refer to the case::

  <f:if condition="{settings.templateLayout} == 10">
      <f:then>
         <f:render partial="List/BrowseList" arguments="{newsItem: newsItem,settings:settings,iterator:iterator}" />
      </f:then>
      <f:else>
         <f:render partial="List/Item" arguments="{newsItem: newsItem,settings:settings,iterator:iterator}" />
      </f:else>
   </f:if>

c. if you want to use the distingtion in the regular plugin you must add it to the TSConfig of the pages


News FlexSlider
---------------
In the Templates/News/News/List.html choose the Partial for the FlexSlider-News

.. code::

   <f:if condition="{settings.templateLayout} == 10">
      <f:then>
         <f:render partial="List/ItemBrowseList" arguments="{newsItem: newsItem,settings:settings,iterator:iterator}" />
      </f:then>
      …


Inside Partials/News/List/ItemBrowseList.html is a cycle viewhelper that puts allways 2 news in on <li></li>

.. code::

  <f:cycle values="{0: '<li>', 1: ''}" as="lis">
    <f:format.raw>{lis}</f:format.raw>
  </f:cycle>

   #partial here

   <f:cycle values="{0: '', 1: '</li>'}" as="endLis">
      <f:format.raw>{endLis}</f:format.raw>
   </f:cycle>


   <f:if condition ="{iterator.isLast}" >
      <f:if condition ="{iterator.isOdd}">
         </li>
      </f:if>
   </f:if>


news prepeared cases for List
-----------------------------
Set in TCEMAIN.tsconfig for the plugin ::

   tx_news.templateLayouts {
      9 = newsLatest
      10 = caseListBrowse
      11 = caseDownloads
   }


About special list-templates for news:
    https://die-schwarzwald-werbeagentur.zazudesign.de/internet-programmierung/news-latest-typo3-extension-news-or-tx_news.html


detail and list view image size
-------------------------------
#folgende Angabe reicht für die Größe -render- die Bilder sind aber in (css news basic).news-single .article .news-img-wrap auf  282 begrenzt

.. code::

   plugin.tx_news {
      settings {
         detail {
            media {
               image {
                  maxWidth = 600
                  maxHeight = 600
               }
            }
         }
         #Same image size then detail so only one image to load, with many
         #news this has to change!
         list {
            media {
               image {
                  maxWidth = 600
                  maxHeight = 600
               }
            }
         }
      }
   }

.. note::

    News list on the normal news-list page has a 2 column backend-layout, but it is the same template behind it like the normal page-layout-template. The second column with the news-aside goes directly inside the news list.html template:
    <f:cObject typoscriptObjectPath="lib.dynamicContent" data="{pageUid: '{data.uid}', colPos: '2'}" />


some news setting stuff
-----------------------

.. code::

 settings {
    # Geänderte CSS verknüpfen:
    cssFile = EXT:templates/Resources/Public/Css/tx_news.css

    # Platzhalterbild entfernen:
    displayDummyIfNoMedia = 0


    # Bildgrößen:
    detail.media.image.maxWidth = 200
    list.media.image.maxWidth = 110
    list.media.image.maxHeight =


    # rel-Attribut für Fancybox vergeben:
    # im Partial "FalMediaImage.html" muss für Fancybox auch die Klasse "fancybox" vergeben werden!
    detail.media.image.lightbox = fancybox

    # Social-Texte übersetzen:
    facebookLocale = de_DE
    googlePlusLocale = de
    disqusLocale = de

    # Social-Links entfernen:
    detail.showSocialShareButtons = 0

    list.paginate.itemsPerPage = 5

    # Datum in URL einfügen:
    link {
      skipControllerAndAction = 1
      hrDate = 1
      hrDate {
        day = j
        month = n
        year = Y
      }
    }
  }
 }

  #in TSconfig
  CAdefaults.sys_file_reference.showinpreview = 1

  plugin.tx_news._LOCAL_LANG.de.more-link = weiter
  plugin.tx_news._LOCAL_LANG.en.more-link = read more
  plugin.tx_news._LOCAL_LANG.fr.more-link = lire la suite

  plugin.tx_news._LOCAL_LANG.de.back-link = zurück zur Übersicht
  plugin.tx_news._LOCAL_LANG.en.back-link = back to list view
  plugin.tx_news._LOCAL_LANG.fr.back-link = retour à la liste



|
|

FlexSlider
==========

FlexSlider has a white border - remove it
-----------------------------------------

.. code::

   .flexslider {
      background: none;
      border: none;
      box-shadow: none;
      margin: 0;
      }


show arrows in mobile sizes
---------------------------
.. code::

   /* take the arrow off-screen on small screens but then
    you don't know that it is a slider on small screens*/

    .d16 .flex-direction-nav .flex-prev {
    left: -60px;
    }

    .d16 .flex-direction-nav .flex-next {
    right: -60px;
    }


put caption on top of image
---------------------------

.. code::

   /* felx-Caption*/
   .d16 .flexslider .slides li {
       position: relative;
   }

   .d16 .flex-caption {
      position: absolute;
      bottom: 0;
      eft: 0;
      display: block;
      padding: 1em;
      font-family: 'noto_sansregular', Arial, Helvetica, sans-serif;
      font-weight: 500;
      background-color: rgba(220, 220, 220, 0.6);
      font-size: 1em;
      line-height: 1.2em;
      width: 70%;
     height: 50%;
   }


|
|

Import/Export of database for the DCEs
======================================

If you want just to import/export the DCE tables -> it is a long story ...

Best is to use phpAdmin and ignore the warnings for the uids.

See also chapter `Some dirty work – to get your DCEs from your TYPO3 standard installation`_


SVGs are still a hassle
=======================

works for the i-world and the rest but gets bigger, svg as background-image

    make them all Base64 encode
    http://b64.io/


works for firefox but not the i-world

    url-encode background-image
    https://yoksel.github.io/url-encoder/


works here and there just optimise

    http://petercollingridge.appspot.com/svg-optimiser














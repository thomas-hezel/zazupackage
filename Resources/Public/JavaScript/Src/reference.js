/* zazudesign – die Schwarzwald Werbeagentur (c) JavaScript 2019
author: Thomas Hezel, Berlin 20190425
popup reference Films
*/


jQuery(document).ready(function ($) {

    function vidCall() {
        overlay = $('.videoOverlay');
        gross = $('.videoBox figure');
        box = $('.videoBox');

        grossW = gross.outerWidth();
        grossH = gross.outerHeight();
        box.css({ 'width': grossW, 'height': grossH });
        overlay.css({ 'display': 'flex', 'justify-content': 'center', 'align-items': 'center' });

        $(window).resize(function () {
            box.css({ 'width': '', 'height': '' });
            overlay.css({ 'display': '', 'justify-content': '', 'align-items': '' });
            grossW = gross.outerWidth();
            grossH = gross.outerHeight();
            box.css({ 'width': grossW, 'height': grossH });
            overlay.css({ 'display': 'flex', 'justify-content': 'center', 'align-items': 'center' });
        });

        $('#closeWinVideo').click(function () {
            overlay.remove();
            $(this).remove();
        });

    };


    $('.ref').click(function () {

        if ($(this).data('intlink')) {
            var intlink = $(this).data('intlink');
            var intwidth = $(this).data('intwidth');
            var intheight = $(this).data('intheight');
            var str = '<div class="videoOverlay"><div class="videoBox"><video width="' + intwidth + '" height="' + intheight + '" autoplay controls><source src="' + intlink + '" type="video/mp4"></video></div></div><div id="closeWinVideo">x</div>';
            $('#socialMedia').after(str);
            vidCall();
        };

        if ($(this).data('extlink')) {
            var extlink = $(this).data('extlink');
            var extwidth = $(this).data('extwidth');
            var extheight = $(this).data('extheight');
            var str2 = '<div class="videoOverlay"><div class="videoBox"><video width="' + intwidth + '" height="' + intheight + '" autoplay controls><source src="' + intlink + '" type="video/mp4"></video></div></div><div id="closeWinVideo">x</div>';
            $('#socialMedia').after(str2);
            vidCall();
        };

        if ($(this).data('i_frame')) {
            var i_frame = $(this).data('i_frame');
            var str3 = '<div class="videoOverlay"><div class="videoBox">' + i_frame + '</div></div><div id="closeWinVideo">x</div>';
            $('#socialMedia').after(str3);
            vidCall();
        };

    });
});


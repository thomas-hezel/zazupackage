/* zazudesign – die Schwarzwald Werbeagentur (c) JavaScript 2017
author: Thomas Hezel, Berlin 20190415/20240529
Leaflet Map JavaScript - variable lati and longi coming from FLUID
*/

jQuery(document).ready(function ($) {


    //in jquery click call for leaflet js
    $('#activateMapBtn').click(function () {
        var markerOrt1 = L.icon({
            iconUrl: '/fileadmin/Resources/Public/Icons/Map/map-berlin.png',
            shadowUrl: '/fileadmin/Resources/Public/Icons/Map/map-berlin-shadow.png',

            iconSize: [133, 35], // size of the icon
            shadowSize: [133, 35], // size of the shadow
            iconAnchor: [133, 35], // point of the icon which will correspond to marker's location
            shadowAnchor: [133, 35],  // the same for the shadow
            popupAnchor: [-6, -76] // point from which the popup should open relative to the iconAnchor
        });

        var markerOrt2 = L.icon({
            iconUrl: '/fileadmin/Resources/Public/Icons/Map/map-winzeln.png',
            shadowUrl: '/fileadmin/Resources/Public/Icons/Map/map-winzeln-shadow.png',

            iconSize: [145, 35], // size of the icon
            shadowSize: [155, 35], // size of the shadow
            iconAnchor: [0, 35], // point of the icon which will correspond to marker's location
            shadowAnchor: [0, 35],  // the same for the shadow
            popupAnchor: [-6, -76] // point from which the popup should open relative to the iconAnchor
        });

        /***
        * last figure here is giving a zoomfactor: 5 is aprox. Europe, 10 is aprox. a region
        ***/
        var map = L.map('map', { zoomControl: false }).setView([lati, longi], 6);

        /*above remove control and put a new one on bottomleft*/
        map.addControl(L.control.zoom({ position: 'bottomleft' }));

        /*tiles from mapBox
            L.tileLayer('https://{s}.tiles.mapbox.com/v3/uhradone.ija63bia/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18
        }).addTo(map);
        */

        /*tiles from openStreetMap*/
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a>',
            maxZoom: 18
        }).addTo(map);

        L.marker([lati1, longi1], { icon: markerOrt1 }).addTo(map).bindPopup("zazu.berlin<br /><b>digital + film</b>");
        L.marker([lati2, longi2], { icon: markerOrt2 }).addTo(map).bindPopup("zazu.winzeln<br /><b>grafik + print</b>");

        //jquery  additions
        $('.dataWarning').addClass('hidden');
        $('#map').addClass('dontShowBackground');


        //on click function
    });


});

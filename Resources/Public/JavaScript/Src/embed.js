// JavaScript Document
// copy right by Thomas Hezel 2019, urherberrechtlich geschützt
// zazu.berlin embed

$(document).ready(function () {
    /*Food Monitor – English original version*/
    var fmonitor = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.foodmonitor.org/videos/food-monitor-video-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'



    /*Tanzania Law original-version*/
    var tlov = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/tanzanian-german-legal-video-ov/tanzanian-german-legal-video-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*Tanzania Law English subtitles:*/
    var tlen = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/tanzanian-german-legal-video-sub-en/tanzanian-german-legal-video-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*Tanzania Law: German subtitles:*/
    var tlde = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/tanzanian-german-legal-video-sub-de/tanzanian-german-legal-video-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'




    /*South African Law original-version*/
    var salov = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/south-african-german-justice-video-ov/south-african-german-justice-video-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*South African Law English subtitles:*/
    var salen = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/south-african-german-justice-video-sub-en/south-african-german-justice-video-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*South African Law: German subtitles:*/
    var salde = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/south-african-german-justice-video-sub-de/south-african-german-justice-video-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'





    /*South African Development original-version:*/
    var sadov = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/south-african-german-development-ov/south-african-german-development-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*South African Development English subtitles:*/
    var saden = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/south-african-german-development-sub-en/south-african-german-development-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*South African Development German subtitles:*/
    var sadde = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/south-african-german-development-sub-de/south-african-german-development-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'



    /*Namibia Logistics original-version:*/
    var nlov = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/namibian-german-logistics-video-ov/namibian-german-logistics-video-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Namibia Logistics English subtitles:*/
    var nlen = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/namibian-german-logistics-video-sub-en/namibian-german-logistics-video-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Namibia Logistics German subtitles:*/
    var nlde = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/namibian-german-logistics-video-sub-de/namibian-german-logistics-video-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'




    /*Ghana Development original-version:*/
    var gdov = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/ghanaian-german-development-video-ov/ghanaian-german-development-video-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Ghana Development English subtitles:*/
    var gden = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/ghanaian-german-development-video-sub-en/ghanaian-german-development-video-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Ghana Development German subtitles:*/
    var gdde = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/ghanaian-german-development-video-sub-de/ghanaian-german-development-video-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'





    /*Kenya Education original-version:*/
    var keov = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/kenya-german-center-educational-ov/kenya-german-center-educational-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Kenya Education English subtitles:*/
    var keen = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/kenya-german-center-educational-sub-en/kenya-german-center-educational-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Kenya Education German subtitles:*/
    var kede = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/kenya-german-center-educational-sub-de/kenya-german-center-educational-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'




    /*Congo Microfinance original-version:*/
    var cmov = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/congolese-german-microfinance-video-ov/congolese-german-microfinance-video-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Congo Microfinance English subtitles:*/
    var cmen = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/congolese-german-microfinance-video-sub-en/congolese-german-microfinance-video-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Congo Microfinance German subtitles:*/
    var cmde = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/congolese-german-microfinance-video-sub-de/congolese-german-microfinance-video-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'




    /*Network Meeting 2016 Ghana English original-version:*/
    var gn = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/network-meeting-ghana.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*Network Meeting 2017 Kenya English original-version:*/
    var ke17 = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/network-meeting-kenya.html" frameborder="0" allowfullscreen></iframe></div></figure>'




    /*Annual Logistics Workshop Namibia English 2017 original-version:*/
    var nlw = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/logistics-workshop-namibia.html" frameborder="0" allowfullscreen></iframe></div></figure>'



    /*Kenya Mining original-version:*/
    var keMinOv = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/kenyan-german-mining-video-ov/kenyan-german-mining-video-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Kenya Mining English subtitles:*/
    var keMinEn = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/kenyan-german-mining-video-sub-en/kenyan-german-mining-video-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Kenya Mining German subtitles:*/
    var keMinDe = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/kenyan-german-mining-video-sub-de/kenyan-german-mining-video-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'




    /*alumni SA Law 12 minutes:*/
    var aluLong = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/alumni-SA-law.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*alumna Charity 40 sec:*/
    var charity = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/alumni-SA-law-Charity-40-seconds.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*alumna Olwethu 40 sec:*/
    var olwethu = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/alumni-SA-law-Olwethu-40-seconds.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*alumnus Selemani 40 sec:*/
    var selemani = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/alumni-SA-law-Selemani-40-seconds.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*alumnus Sosteness 40 sec:*/
    var sosteness = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/alumni-SA-law-Sosteness-40-seconds.html" frameborder="0" allowfullscreen></iframe></div></figure>'


      /*alumna Faustine Wabwire 80 sec*/
      var faustine = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/faustine-wabwire-short-video-ov/faustine-wabwire-short-video-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'



    /*Alumni Meeting SA 2017 original-version:*/
    var alSA17Ov = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/alumni-meeting-sa-ov/alumni-meeting-sa-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Alumni Meeting SA 2017 English subtitles:*/
    var alSA17En = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/alumni-meeting-sa-sub-en/alumni-meeting-sa-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*Alumni Meeting SA 2017 German subtitles:*/
    var alSA17De = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.digiface.org/videos/alumni-meeting-sa-sub-de/alumni-meeting-sa-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /**
     * From here bigger size! max-width:960px and padding-bottom: 58.95%, video 960x540 and MPEG-DASH/HLS
     * */

    /*Alumni Meeting SA 2017 long-version:*/
    var alSA17Lv = '<figure style="max-width:1920px;margin:0"><div style="position:relative;padding-bottom:56.25%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="1920" height="1080" src="https://www.african-excellence.de/videos/alumni-meeting-SA-long-version.html" frameborder="0" allowfullscreen></iframe></div></figure>'


    /*West African Governance original-version:*/
    var waGovOv = '<figure style="max-width:960px;margin:0"><div style="position:relative;padding-bottom:58.95%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="960" height="540" src="https://www.digiface.org/videos/west-african-german-governance-video-ov/west-african-german-governance-video-ov.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*West African Governance English subtitles:*/
    var waGovEn = '<figure style="max-width:960px;margin:0"><div style="position:relative;padding-bottom:58.95%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="960" height="540" src="https://www.digiface.org/videos/west-african-german-governance-video-sub-en/west-african-german-governance-video-sub-en.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*West African Governance German subtitles:*/
    var waGovDe = '<figure style="max-width:960px;margin:0"><div style="position:relative;padding-bottom:58.95%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="960" height="540" src="https://www.digiface.org/videos/west-african-german-governance-video-sub-de/west-african-german-governance-video-sub-de.html" frameborder="0" allowfullscreen></iframe></div></figure>'



    /*DIGIFACE kick-off meeting 2020 March SA original-version:*/
    var digKickOffOv = '<figure style="max-width:960px;margin:0"><div style="position:relative;padding-bottom:58.95%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="960" height="540" src="https://www.african-excellence.de/videos/digiface-kick-off-meeting-sa-video-original-version.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*DIGIFACE kick-off meeting 2020 March SA short-version:*/
    var digKickOffSv = '<figure style="max-width:960px;margin:0"><div style="position:relative;padding-bottom:58.95%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="960" height="540" src="https://www.african-excellence.de/videos/digiface-kick-off-meeting-sa-video-short-version.html" frameborder="0" allowfullscreen></iframe></div></figure>'

    /*DIGIFACE kick-off meeting 2020 March SA French-version:*/
    var digKickOffFv = '<figure style="max-width:960px;margin:0"><div style="position:relative;padding-bottom:58.95%;padding-top:10px;height:0;"><iframe id="iframeZazu" style="position:absolute;top:0;left:0;width:100%;height:100%;" width="960" height="540" src="https://www.african-excellence.de/videos/digiface-kick-off-meeting-sa-video-french-version.html" frameborder="0" allowfullscreen></iframe></div></figure>'



    $("select").change(function () {
        var str = "";
        $("select option:selected").each(function () {
            str += $(this).text();

            switch (str) {
                case "Food Monitor – English original version":
                    str = fmonitor;
                    break;

                case "Tanzania Law – original version":
                    str = tlov;
                    break;
                case "Tanzania Law – English subtitles":
                    str = tlen;
                    break;
                case "Tanzania Law – German subtitles":
                    str = tlde;
                    break;


                case "South Africa Law – original version":
                    str = salov;
                    break;
                case "South Africa Law – English subtitles":
                    str = salen;
                    break;
                case "South Africa Law – German subtitles":
                    str = salde;
                    break;


                case "South Africa Development – original version":
                    str = sadov;
                    break;
                case "South Africa Development – English subtitles":
                    str = saden;
                    break;
                case "South Africa Development – German subtitles":
                    str = sadde;
                    break;


                case "Namibia Logistics – original version":
                    str = nlov;
                    break;
                case "Namibia Logistics – English subtitles":
                    str = nlen;
                    break;
                case "Namibia Logistics – German subtitles":
                    str = nlde;
                    break;


                case "Kenya Education – original version":
                    str = keov;
                    break;
                case "Kenya Education – English subtitles":
                    str = keen;
                    break;
                case "Kenya Education – German subtitles":
                    str = kede;
                    break;


                case "Congo Microfinance – original version":
                    str = cmov;
                    break;
                case "Congo Microfinance – English subtitles":
                    str = cmen;
                    break;
                case "Congo Microfinance – German subtitles":
                    str = cmde;
                    break;


                case "Ghana Development – original version":
                    str = gdov;
                    break;
                case "Ghana Development – English subtitles":
                    str = gden;
                    break;
                case "Ghana Development – German subtitles":
                    str = gdde;
                    break;


                case "Ghana Network Meeting 2016 – original version":
                    str = gn;
                    break;

                case "Kenya Network Meeting 2017 – original version":
                    str = ke17;
                    break;

                case "Namibia Logistics Workshop 2017 – original version":
                    str = nlw;
                    break;


                case "Kenya Mining – original version":
                    str = keMinOv;
                    break;
                case "Kenya Mining – English subtitles":
                    str = keMinEn;
                    break;
                case "Kenya Mining – German subtitles":
                    str = keMinDe;
                    break;


                case "alumni SA Law – long version":
                    str = aluLong;
                    break;

                case "alumna Charity Wibabara 40 sec":
                    str = charity;
                    break;
                case "alumna Olwethu Majola 40 sec":
                    str = olwethu;
                    break;
                case "alumnus Selemani Kinyunyu 40 sec":
                    str = selemani;
                    break;
                case "alumnus Sosteness Materu 40 sec":
                    str = sosteness;
                    break;
                case "alumna Faustine Wabwire 80 sec":
                    str = faustine;
                    break;


                case "alumni meeting SA 2017 – original version":
                    str = alSA17Ov;
                    break;
                case "alumni meeting SA 2017 – English subtitles":
                    str = alSA17En;
                    break;
                case "alumni meeting SA 2017 – German subtitles":
                    str = alSA17De;
                    break;
                case "alumni meeting SA 2017 – long version":
                    str = alSA17Lv;
                    break;

                case "West African Local Governance – original version":
                    str = waGovOv;
                    break;
                case "West African Local Governance – English subtitles":
                    str = waGovEn;
                    break;
                case "West African Local Governance – German subtitles":
                    str = waGovDe;
                    break;

                case "DIGIFACE kick-off meeting - original version":
                    str = digKickOffOv;
                    break;
                case "DIGIFACE kick-off meeting - short version":
                    str = digKickOffSv;
                    break;
                case "DIGIFACE kick-off meeting - French version":
                    str = digKickOffFv;
                    break;

                default:
                    str = "";
            }

        });

        $('#iframeCode').text(str);
    }).change();
});

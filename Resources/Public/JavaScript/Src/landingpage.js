"use strict";

/**
 * load lardger images only with JS
 * to avoid to much pre-loading
 * one image is loaded and then the other through navigation
 */


// target div
const container = document.getElementById('container');
const content = document.getElementById('content');
// content plus navigation
const popUp = document.getElementById('popUp');


// create image tag
const newImg = document.createElement('img');
newImg.alt = 'Referenzbild';

// get close btn
const closeBtn = document.getElementsByClassName('closeBtn');
for (let i = 0; i < closeBtn.length; i++) {
    closeBtn[i].addEventListener('click', () => {
        // Hide the target div by setting its display to none
        container.style.display = 'none';
    });
}



let index = "";
let total = "";
let source = "";
let collectionImg = null;
let scrollTop = 0;

// click function
const onClick = (event) => {
    // get the uid of the specific collection (DCE container element)
    const uid = event.srcElement.getAttribute("data-uid");
    // get all images of this specific collection (container element)
    collectionImg = document.getElementsByClassName(`refImg-${uid}`);
    // now inside the collection
    index = event.srcElement.getAttribute("data-index");
    total = event.srcElement.getAttribute("data-total");
    // get the uri of the full img
    source = event.srcElement.getAttribute("data-src");
    // add image uri
    newImg.src = source;
    console.log(source);
    //remove old from dom
    while (content.firstChild) {
        content.removeChild(content.firstChild);
    }
    // add new child
    content.appendChild(newImg);

    // display container
    container.style.display = 'block';
    // move popup to now visible area
    // scrollTop == position mouse
    scrollTop = event.pageY;
    let position = scrollTop - 200;
    popUp.style.marginTop = `${position}px`;

}


// click on forward
const onForward = (event) => {
    // use the index of the first clicked img and go to the next
    if (Number(index) < (Number(total) - 1)) {
        let indexNr = Number(index) + 1;
        index = String(indexNr);
        source = collectionImg[index].getAttribute("data-src");
    } else {
        // at the end of the collection go back to the first img
        source = collectionImg[0].getAttribute("data-src");
        index = 0;
    }
    newImg.src = source;
    //remove old from dom
    while (content.firstChild) {
        content.removeChild(content.firstChild);
    }
    // add new child
    content.appendChild(newImg);
}


// click on backwards
const onBack = (event) => {
    if (Number(index) > 0) {
        let indexNr = Number(index) - 1;
        index = String(indexNr);
        source = collectionImg[index].getAttribute("data-src");
    } else {
        source = collectionImg[0].getAttribute("data-src");
        index = Number(total) - 1;
    }
    newImg.src = source;
    //remove old from dom
    while (content.firstChild) {
        content.removeChild(content.firstChild);
    }
    // add new child
    content.appendChild(newImg);
}

// add click event listener on forward
const navRight = document.getElementById('navRight');
navRight.addEventListener('click', onForward);


// add click event listener on backwards
const navLeft = document.getElementById('navLeft');
navLeft.addEventListener('click', onBack);

// add click function to all imges of all collection (DCE container collections)
const allImg = document.getElementsByClassName('allImg');
for (let i = 0; i < allImg.length; i++) {
    allImg[i].addEventListener('click', onClick);
}

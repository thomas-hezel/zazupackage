<?php
/**
 * Created by
 * User: thomas hezel
 * Date: 2019-05-04
 * Time: 18:26
 *
 * Autoload for classes, this shows in the Backend e.g. version
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'Sitepackage by zazu.berlin',
    'description' => 'Sitepackage zazu.berlin',
    'category' => 'templates',
    'author' => 'Thomas Hezel',
    'author_email' => 'info@zazu.berlin',
    'author_company' => 'zazu.berlin',
    'version' => '1.0.0',
    'state' => 'stable',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.4.99',
            'fluid_styled_content' => '10.4.0-10.4.99',
        ],
        'conflicts' => [
        ],
    ],
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1
];

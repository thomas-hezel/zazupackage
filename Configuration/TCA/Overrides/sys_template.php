<?php
/**
 * Created by zazu.berlin
 * Author: thomas hezel
 * Date: 2019-02-21
 * This adds automatically to static templates in root template
 * all Typoscript that is in the TypoScript folder
 */

defined('TYPO3') || die();

call_user_func(function()
{
    /**
     * Extension key
     */
    $extensionKey = 'zazupackage';

    /**
     * Default TypoScript
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'Sitepackage'
    );

});
